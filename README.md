# pubsweet-test-docker-base

This is the repo for the PubSweet testing base docker image.

The image is used for Gitlab CI testing for all PubSweet repos, and can also be used for local debugging of tests, using exactly the same environment that will run on the CI server.

## Pre-requisites

To do anything with the image you'll need to [install docker](https://docs.docker.com/engine/installation/).

To debug GitlabCI runs locally you'll need to [install gitlab-ci-multi-runner](https://docs.gitlab.com/runner/install/).

## Using the prebuilt image

```bash
# pull the prebuilt image from dockerhub
docker pull pubsweet/pubsweet-test-base
# run the `test` script defined in your .gitlab-ci.yml file inside the image
gitlab-ci-multi-runner exec docker --docker-image=pubsweet/pubsweet-test-base test
```

Your `test` script inside .gitlab-ci.yml should look like this:

```
image: pubsweet/pubsweet-test-base

test:
  script:
    - xvfb-run --server-args "-screen 0 1024x768x24" npm run test
```

The `xvfb-run` command runs the final argument (here `npm run test`) in an environment that uses a framebuffer to simulate an X-window system. This is necessary for most 'headless' browsers to run on a system that is actually headless.

## Helper scripts

### `build.sh` - Building the image from Dockerfile, using cached parts

Docker can rebuild an image by reusing unchanged parts of any previous builds.

To do this:

```bash
./build.sh
```

### `cleanbuild.sh` - Building the image from Dockerfile, without caching

To rebuild the image completely from scratch, without re-using any cached parts of previous builds:

```bash
./cleanbuild.sh
```

### `spawn.sh` - Create a new container and connect to it

To create a new container instance of the image and connect to a terminal running inside the container:

```bash
./spawn.sh
```

This is useful if you want try different things inside a clean version of the image.

### `connect.sh` - Connect to a running instance

To attach to a terminal inside an already running container:

```bash
docker container list
# select the containerID you want to attach to
./connect.sh containerID
```

This is useful if there's a test run already in progress in a container and you want to debug what's happening in that specific container.

## What's in the image?

This image comes prepped with:

- electron / nightmareJS preconfigured to run inside Docker
- the three major pubsweet repos (backend, frontend and cli) pre-installed
- their dependencies cached and npm setup to use the cached dependencies

Altogether this should simplify and speed up testing of PubSweet apps.

## Credits

This image is based on the [fentas/docker-nightmare](https://github.com/fentas/docker-nightmare) image that solves all the issues around running headless electron (via [nightmareJS](http://www.nightmarejs.org/)) inside a Docker image.
